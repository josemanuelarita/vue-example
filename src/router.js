import Vue from "vue";
import VueRouter from "vue-router";

import PageHome from "./pages/Home";
import PageNotFound from "./pages/NotFound";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: PageHome
    },
    {
      path: "*",
      component: PageNotFound
    }
  ]
});

export default router;
